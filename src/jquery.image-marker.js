;(function( $, window, document, undefined ) {

  'use strict';

    // defining vars in the begining of every function to predict hoisting
    // define vars separatelly to avoid mistakes with comas and semicolons
    var pluginName = 'imageMarker';
    var defaults = {drag_disabled: false};

    function Plugin ( element, options ) {
      this.$element = $(element);
      this.settings = $.extend( {}, defaults, options );
      this._defaults = defaults;
      this._name = pluginName;
      this.$left_box = null;
      this.$right_box = null;
      this.$drag_box = null;
      this.init();
    }

    $.extend( Plugin.prototype, {
      init: function() {
        this.renderMarkup();
        this.bindListeners();
      },

      // preparing the markup for insertion into target element, using $ prefix for jquery objects
      // using BEM for class naming (block__element--modifier), control visual part with updating css
      renderMarkup: function() {

        var self = this;

        var $container =                    $('<div class="image-marker-container"></div>');
        var $left_box_wrapper =             $('<div class="image-marker-container__box image-marker-container__box--left"></div>');

        // linking element for fast manipulation in future (adding markers, checking height), so no need for searching for element again
        var $left_box =  this.$left_box =   $('<div class="image-marker-container__box__content"></div>');
        var $right_box_wrapper =            $('<div class="image-marker-container__box image-marker-container__box--right"></div>');
        var $right_box = this.$right_box =  $('<div class="image-marker-container__box__content"></div>');
        var $image_box =                    $('<div class="image-marker-container__box image-marker-container__box--image"></div>');
        var $drag_box = this.$drag_box =    $('<div class="image-marker-container__box image-marker-container__box--drag"></div>');
        var $image =                        $('<img class="image-marker-container__img"/>');

        $left_box_wrapper.append($left_box);
        $right_box_wrapper.append($right_box);

        $([$left_box, $right_box]).sortable({
          connectWith: '.image-marker-container__box__content',
          opacity: 0.4,
          cursor: 'move',
          tolerance: 'pointer',
          receive: function(e, ui) {
            var marker = $(ui.item).data('marker');
            if ($(this).height() > $(this).parent().height()) return ui.sender.sortable('cancel');
            marker.col = ($(ui.sender).parent().hasClass('image-marker-container__box--left')) ? 2 : 1;
          },
          start: function(e, ui) {
            // var $dot = $(ui.item).data('dot');
            // var $line = $(ui.item).data('line');
            // $dot.css({visibility: 'hidden'});
            // $line.css({visibility: 'hidden'});
          },
          stop: function(e, ui) {
            // var $dot = $(ui.item).data('dot');
            // var $line = $(ui.item).data('line');
            // $dot.css({visibility: 'visible'});
            // $line.css({visibility: 'visible'});
            self.$element.trigger('drag_all');
            self.$element.trigger('drag_all');
          }
        });

        $image.attr('src', this.settings.src);
        $image_box.append($image).append($drag_box);

        // bundling the markup into vitrual container
        $container.append($left_box_wrapper).append($right_box_wrapper).append($image_box);

        // inserting virtual container as a last step regarding to performance issues (slow dom, reflow, repaint)
        this.$element.append($container);
      },

      // adding listeners for client to trigger plugin functions
      bindListeners: function() {
        this.$element.on('add_marker', (function(e, data) {
          this.addTextbox(data);
        }).bind(this)); // binding context to avoid closure in parent function (var self = this)
        this.$element.on('get_markers', (function(e, cb) {
          var getOrderedMarkers = function(idx, el) {
            var marker = $(el).data('marker');
            marker.order = idx;
            return marker;
          };
          var sort = function(a, b) { return a - b; };
          var left_sorted_markers = this.$left_box.find('.image-marker__text-box').map(getOrderedMarkers).sort(sort).toArray();
          var right_sorted_markers = this.$right_box.find('.image-marker__text-box').map(getOrderedMarkers).sort(sort).toArray();
          var all_sorted_markers = left_sorted_markers.concat(right_sorted_markers);

          cb(all_sorted_markers);

        }).bind(this));
      },

      // adding text box
      addTextbox: function(marker) {

        // all manipulations with virtual element should be done before insertion regarding to performance issues
        var $text_box = $('<div class="image-marker__text-box"></div>');
        var $dot = $('<div class="image-marker__dot"></div>');
        var $line = $('<div class="image-marker__line"></div>');
        var $left_box = this.$left_box;
        var onDrag = function() {
              var text_box_offset = ($text_box.parent()[0] == $left_box[0]) ? ($text_box.parent().width() - 4) : 4;
              var x1 = $text_box.offset().left + text_box_offset;
              var x2 = $dot.offset().left + ($dot.width() / 2);
              var y1 = $text_box.offset().top + ($text_box.height() / 2);
              var y2 = $dot.offset().top + ($dot.height() / 2);
              var hypotenuse = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
              var angle = Math.atan2((y1 - y2), (x1 - x2)) *  (180 / Math.PI);

              if (angle >= 90 && angle < 180) {
                  y1 = y1 - (y1 - y2);
              }
              if (angle > 0 && angle < 90) {
                  x1 = x1 - (x1 - x2);
                  y1 = y1 - (y1 - y2);
              }
              if (angle <= 0 && angle > -90) {
                  x1 = x1 - (x1 - x2);
              }

              $line.queue(function() {
                  $(this).offset({top: y1, left: x1});
                  $(this).dequeue();
              }).queue(function() {
                  $(this).width(hypotenuse);
                  $(this).dequeue();
              }).queue(function() {
                  $(this).rotate(angle);
                  $(this).dequeue();
              });

              marker.pos.x = parseInt($dot.css('left'));
              marker.pos.y = parseInt($dot.css('top'));
           };

        var $title = $('<h2 class="image-marker__text-box__title" contenteditable="true">Title</h2>');
        if (!!marker.title) $title.text(marker.title);
        $title.on('change keydown paste input', function() {
          marker.title = $title.text();
        });
        $text_box.append($title);

        var $content = $('<p class="image-marker__text-box__content" contenteditable="true">Content</p>');
        if (!!marker.content) $content.text(marker.content);
        $content.on('change keydown paste input', function() {
          marker.content = $content.text();
        });
        $text_box.append($content);

        if (!!marker.className) {
          $text_box.addClass(marker.className);
          $dot.addClass(marker.className);
          $line.addClass(marker.className);
        }

        $text_box.data('dot', $dot);
        $text_box.data('line', $line);

        if (marker.col == 1) {
          return this.mountTo(this.$left_box, $text_box, $dot, $line, onDrag, marker);
        } else if (marker.col == 2) {
          return this.mountTo(this.$right_box, $text_box, $dot, $line, onDrag, marker);
        } else {
          if (this.mountTo(this.$left_box, $text_box, $dot, $line, onDrag, marker)) return marker.col = 1;
          if (this.mountTo(this.$right_box, $text_box, $dot, $line, onDrag, marker)) return marker.col = 2;
        }
      },

      mountTo: function($target, $text_box, $dot, $line, onDrag, marker) {

        // hidding element, but still displaying for future measurements
        $text_box.css({visibility: 'hidden'});
        $target.append($text_box);

        if ($target.height() > $target.parent().height()) {
          $text_box.remove();
          return false;
        }

        if (!this.settings.drag_disabled) {
          var $close_btn = $('<div class="image-marker__text-box__close-btn">X</div>');
          $close_btn.on('click', function() {
            $text_box.remove();
            $dot.remove();
            $line.remove();
            this.$element.trigger('drag_all');
            this.$element.trigger('drag_all');
          }.bind(this));
          $text_box.append($close_btn);
        }

        $text_box.css({visibility: 'visible'});

        var drawDot = (function() {
          if (!marker.pos) {
            marker.pos = {
              d: 20
            };
          }

          if (!marker.pos.x || !marker.pos.y) {
            marker.pos.x = ($text_box.parent()[0] == this.$left_box[0]) ? 0 : this.$drag_box.width() - marker.pos.d;
            marker.pos.y = $text_box.offset().top - $text_box.parent().offset().top + $text_box.height() / 2 - marker.pos.d / 2;
          }

          $dot.css({
            width: marker.pos.d + 'px',
            height: marker.pos.d + 'px',
            top: marker.pos.y + 'px',
            left: marker.pos.x + 'px'
          });
        }).bind(this);

        drawDot();

        $dot.on('dblclick', function() {
          marker.pos.d = (marker.pos.d >= 60) ? 20 : marker.pos.d * 2;
          drawDot();
          onDrag();
          onDrag();
        });

        this.$drag_box.append($line).append($dot);

        $dot.draggable({
          containment: 'parent',
          distance: 0,
          delay: 0,
          disabled: this.settings.drag_disabled
        }, {
          drag: onDrag
        });

        this.$element.on('drag_all', onDrag);
        $.data($text_box[0], 'marker', marker);

        onDrag();
        onDrag();

        return true;
      }
    });

    $.fn[ pluginName ] = function( options ) {
      return this.each(function() {
        if ( !$.data( this, 'plugin_' + pluginName ) ) $.data( this, 'plugin_' + pluginName, new Plugin( this, options ) );
      });
    };

    $.fn.rotate = function(degrees) {
      $(this).css({'transform' : 'rotate('+ degrees +'deg)'});
      return $(this);
    };

})( jQuery, window, document );

// todo: remove $.fn.rotate
